#!/usr/bin/env bash
#########################
# This script is for reference. Not being used
# This script manages the xpresso environment locally.
########################

USERNAME="admin"
PASSWORD="Abz00ba@123"

DOCKER_HOSTNAME="xpresso"
DOCKER_NAME="xpresso_devenv"
DOCKER_REGISTRY="dockerregistry.xpresso.ai"
DOCKER_IMAGE="dockerregistry.xpresso.ai/library/xpresso_local:latest"
APP_FOLDER="/app"

mounts=""
command=""
if [[ -z "${PROJECT_NAME}" ]]; then
  project_name="carters_project"
else
  project_name=${PROJECT_NAME}
fi

if [[ -z "${COMPONENT_NAME}" ]]; then
  component_name="{{XPRESSO_COMPONENT_NAME}}"
else
  component_name=${COMPONENT_NAME}
fi

base_image=${DOCKER_IMAGE}

init(){
    container_name="${DOCKER_NAME}"
    xpresso_host_prefix="xpresso"
    image_name="${DOCKER_REGISTRY}/xprops/local_${HOSTNAME}/xpresso_local_env:latest"
    if [[ ${project_name} != "" ]];then
      container_name="${container_name}_${project_name}"
      xpresso_host_name="${xpresso_host_prefix}_${project_name}"
      image_name="${DOCKER_REGISTRY}/xprops/${project_name}/base/xpresso_local_env:latest"
    fi
    if [[ ${component_name} != "" ]];then
      container_name="${container_name}_${project_name}_${component_name}"
      xpresso_host_name="${xpresso_host_prefix}_${project_name}_${component_name}"
      image_name="${DOCKER_REGISTRY}/xprops/${project_name}/base/${component_name}/xpresso_local_env:latest"
    fi

    if [[ ${base_image} == "" ]];then
      base_image=${DOCKER_IMAGE}
    fi

    echo "Container name: ${container_name}"
    echo "Base image: ${base_image}"
    echo "Updated image name: ${image_name}"
    echo "Xpresso host name: ${xpresso_host_name}"

}

push(){

  # Login to docker
  docker login dockerregistry.xpresso.ai -u admin -p Abz00ba@123
  echo "Pushing your image: [${image_name}] to the xpresso registry"
  docker push ${image_name}
}

start(){


    echo "Validating the workspace"
    if [[ "$(docker images -q ${image_name} 2> /dev/null)" == "" ]]; then

      # Login to docker
      docker login dockerregistry.xpresso.ai -u admin -p Abz00ba@123
      docker pull ${base_image} 2> /dev/null
      docker tag ${base_image} ${image_name} 2> /dev/null
    fi

    options="s"
    if [[ "$(docker ps -a | grep ${container_name} 2> /dev/null)" != "" ]]; then
      echo -n "Previous container is still running. Do you want to exec into it/exit existing and start new container/ignore?[e/s/i]: "
      read options
      if [[ ${options} == "s" ]];then
        docker rm -f ${container_name}  2> /dev/null
      elif [[ ${options} == "e" ]];then
        if [[ "$( docker container inspect -f '{{.State.Running}}' ${container_name} )" == "true" ]]; then
          docker exec -it ${container_name} /bin/bash
        else
          echo "Error: Docker container is not running. Please use option 's'. Exiting..."
          exit 0
        fi
      else
        echo "Nothing to do now. Exiting"
        exit 0
       fi
    fi


    if [[ ${options} == "s" ]];then
      echo "Initiating the workspace"
      if [[ ${mounts} == "" ]];then
        docker run -it \
          --hostname=${xpresso_host_name} \
          --privileged \
          -e PROJECT_NAME=${PROJECT_NAME} \
          -e COMPONENT_NAME=${COMPONENT_NAME} \
          --name=${container_name} \
          --net=host \
          -v /var/run/docker.sock:/var/run/docker.sock \
          -v ${PWD}:${APP_FOLDER} \
          ${image_name}
      else
        docker run -it \
            --hostname=${xpresso_host_name} \
            --privileged \
            -e PROJECT_NAME=${PROJECT_NAME} \
            -e COMPONENT_NAME=${COMPONENT_NAME} \
            --name=${container_name} \
            --net=host \
            -v /var/run/docker.sock:/var/run/docker.sock \
            -v ${PWD}:${APP_FOLDER} \
            -v ${mounts} \
            ${image_name}
      fi
    fi

    echo -n "Do you want to persist the changes?[y/n]: "
    read yesorno
    if [[ ${yesorno} == "n" ]];then
      echo "Nothing to do now. Exiting"
      exit 0
    fi

    echo "Persisting your workspace"
    old_image_id=`docker images ${image_name} -q`
    docker commit ${container_name} ${image_name} &> /dev/null

    echo ""
    echo "Workspace has been saved here --> ${image_name}"
    echo ""
    echo "Note: You can now use this as your base image in your Dockerfile"

    docker rmi ${old_image_id} 2> /dev/null | true
    exit 0
}

while [[ $# -gt 0 ]]; do
   if [[ $1 == *"--"* ]]; then
        param="${1/--/}"
        declare $param="$2"
   fi
  shift
done

init
if [[ ${command} == "start" ]];then
  start
elif [[ ${command} == "push" ]];then
  push
else
  echo "Error: Provide correct parameters: bash start-env.sh "
  echo ""
  echo "Supported Command: "
  echo " --command : start/push: Start the workspace for local development"
  echo "                         Push the workspace to the xpresso"
  echo " --mounts: additional mounts"
fi
exit 0
